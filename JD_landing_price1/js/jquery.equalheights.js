(function($){
	$.fn.equalHeights=function(minHeight,maxHeight){
		tallest=(minHeight)?minHeight:0;
		this.each(function(){
			if($(this).height()>tallest){tallest=$(this).height()}
		});
		if((maxHeight)&&tallest>maxHeight) tallest=maxHeight;
		return this.each(function(){$(this).height(tallest)})
	}
})(jQuery)

$(window).load(function(){
	if($(document).width()>767){
		if($(".tabs_list_item").length){$(".tabs_list_item").equalHeights()}
	}
})
$(window).resize(function(){
	$(".tabs_list_item").css({height:'auto'});
	if($(document).width()>767){
		if($(".tabs_list_item").length){$(".tabs_list_item").equalHeights()}
	}
})